﻿// ReSharper disable ConvertToConstant.Local

namespace Literals
{
    public static class Floats
    {
        public static float ReturnFloat31()
        {
            // TODO #3-1. Return "0" literal.
            return 0f;
        }

        public static float ReturnFloat32()
        {
            // TODO #3-2. Return "1.01" literal.
            return 1.01f;
        }

        public static float ReturnFloat33()
        {
            // TODO #3-3. Return "-0.01" literal.
            return -0.01f;
        }

        public static float ReturnFloat34()
        {
            // TODO #3-4. Return "1,048,294,829,438,549,029,840,452,834.109492298482" literal.
            return 1048294829438549029840452834.109492298482f;
        }

        public static float ReturnFloat35()
        {
            // TODO #3-5. Return "-30,492,996,837,502,378,502,387,459,850,243.942692284652825" literal.
            return -30492996837502378502387459850243.942692284652825f;
        }

        public static float ReturnFloat36()
        {
            // TODO #3-6. Return "-0.000000000000000000000000000000000000000123" literal.
            return -0.000000000000000000000000000000000000000123f;
        }

        public static float ReturnFloat37()
        {
            // TODO #3-7. Return "-1.23E-40" literal.
            return -1.23E-40f;
        }

        public static float ReturnFloat38()
        {
            // TODO #3-8. Return "1,048,294,829,438,549,029,840,452,834.109492298482" literal.
            return 1048294829438549029840452834.109492298482f;
        }

        public static float ReturnFloat39()
        {
            // TODO #3-9. Return "-30,492,996,837,502,378,502,387,459,850,243.942692284652825" literal.
            return -30492996837502378502387459850243.942692284652825f;
        }

        public static float ReturnFloat310()
        {
            // TODO #3-10. Return "0.6 + 0.1" expression.
            return 0.699999988f;
        }
    }
}
