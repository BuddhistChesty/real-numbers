﻿namespace Literals
{
    public static class Doubles
    {
        public static double ReturnDouble41()
        {
            // TODO #4-1. Return "0.0" literal.
            return 0.0d;
        }

        public static double ReturnDouble42()
        {
            // TODO #4-2. Return "0.0001" literal.
            return 0.0001;
        }

        public static double ReturnDouble43()
        {
            // TODO #4-3. Return "-10,000.0000000001" literal.
            return -10000.0000000001;
        }

        public static double ReturnDouble44()
        {
            // TODO #4-4. Return "1,048,294,829,438,549,029,840,452,834.109492298482" literal.
            return 1.0482948294385491E+27d;
        }

        public static double ReturnDouble45()
        {
            // TODO #4-5. Return "-30,492,996,837,502,378,502,387,459,850,243.942692284652825" literal.
            return -30492996837502378502387459850243.942692284652825;
        }

        public static double ReturnDouble46()
        {
            // TODO #4-6. Return "0.6 + 0.1" expression.
            return 0.7;
        }
    }
}
